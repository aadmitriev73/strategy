package com.dmitriev.strategy.pattern.printer;

public class ConsolePrinter implements IPrinter {

	@Override
	public void print(String text) {
		System.out.println(text);
	}
}
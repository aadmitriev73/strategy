package com.dmitriev.strategy.pattern;

import com.dmitriev.strategy.pattern.printer.ConsolePrinter;
import com.dmitriev.strategy.pattern.printer.FilePrinter;
import com.dmitriev.strategy.pattern.printer.IPrinter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {

		IPrinter printer = null;
		String text = args[0];
		int	number = Integer.parseInt(args[1]);

		switch (number)
		{
			case 0:
				printer = new ConsolePrinter();
				printer.print(text);
				break;
			case 1:
				printer = new FilePrinter();
				printer.print(text);
				break;
		}
	}
}
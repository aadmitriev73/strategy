package com.dmitriev.strategy.pattern.printer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class FilePrinter implements IPrinter {

	private final String directory = System.getProperty("user.dir");

	@Override
	public void print(String text) throws IOException {
		File outFile = new File(directory,"log.txt");
		try(BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8")))
		{
			out.write(text);
		}
	}
}
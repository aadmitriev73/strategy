package com.dmitriev.strategy.pattern.printer;

import java.io.IOException;

public interface IPrinter
{
	void print( String text ) throws IOException;
}